function display_key_value(key, value, lvl)
% Display a key/value pair with indentation
    global internal_handles_displayed;

    if nargin < 2,
        lvl = 0;
    end
    indent = blanks(lvl*4);
    
    switch class(value)
        case 'char'
            fprintf('%s  %12s: ''%s''\n', indent, key, value);
        case 'double'
            if length(value) == 1,
                value = sprintf('%g', value);
            else
                if size(value,1) == 1,
                    value = ['[ ' sprintf('%g ', value) ']'];
                elseif isempty(value),
                    value = '[]';
                else
                    tmp = '[';
                    for k = 1:size(value, 1),
                        if k>1,
                            tmp = [ tmp '\n' sprintf('%s %12s    ', indent, '') ];
                        end
                        tmp = [tmp '[ ' sprintf('%g ', value(k,:)) ']'];
                    end
                    tmp = [ tmp ']'];
                    value = sprintf(tmp);
                end
            end
            fprintf('%s  %12s: %s\n', indent, key, value);
        case 'function_handle'
            fprintf('%s  %12s: %s\n', indent, key, func2str(value));
        case 'cell'
            fprintf('%s  %12s: {\n', indent, key);
            
            N = length(value);
            if N > 0,
                fmt = sprintf('%12s   [%%%dd] (%%s)', '', floor(log10(N))+1);

                for k = 1:length(value),
                    display_key_value(sprintf(fmt,k,class(value{k})), value{k}, lvl+1);
                end
            end
            fprintf('%s %12s   }\n', indent, '');
        otherwise
            if isenum(value),
                fprintf('%s  %12s: %s (%s)\n', indent, key, value.char, class(value));
            else
                idx = [];
                for k = 1:length(internal_handles_displayed),
                    if internal_handles_displayed{k} == value,
                        idx = k;
                        break;
                    end
                end
                
                if ~isempty(idx),
                    fprintf('%s  %12s: <see above>\n', indent, key);
                else
                    internal_handles_displayed{end+1} = value;
                    fprintf('%s  %12s:\n', indent, key);
                    display_object_fields(value, lvl+1);
                end
            end
    end
end