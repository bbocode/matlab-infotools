function display_object_fields(obj, lvl)
% Display all fields of an object
    global internal_handles_displayed;

    if nargin < 2,
        lvl = 1;
        internal_handles_displayed = {};
    end

    fn = fieldnames(obj);
    for k=1:length(fn),
        key = fn{k};
        value = obj.(key);
        display_key_value(key, value, lvl);
    end
end