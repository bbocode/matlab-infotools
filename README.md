# Matlab informational tools #

## Tools ##

### infopanel ###

A GUI with a text area (to display informational or debugging text), status and progress bar (useful as an alternative to 'waitbar' which does not block MATLAB by requiring to stay in the foreground).
