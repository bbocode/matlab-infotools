function varargout = infopanelgui(varargin)
% Info panel
%
% USAGE
% win = infopanelgui() - start and get window

% Edit the above text to modify the response to help infopanelgui

% Last Modified by GUIDE v2.5 23-Jun-2016 19:50:20

% Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @infopanelgui_OpeningFcn, ...
                       'gui_OutputFcn',  @infopanelgui_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
% End initialization code - DO NOT EDIT
end

function infopanelgui_OpeningFcn(hObject, eventdata, handles, varargin)
    global oippriv

    handles.xui = xui.Control(handles);    
    handles.output = hObject;
    guidata(hObject, handles);
    
    if ~isfield(oippriv, 'handles')
        set_figure_position(handles.infopanelgui, [0 0]);
        
        drawnow
        jinfo = findjobj(handles.info); % undocumented Matlab: http://undocumentedmatlab.com/blog/setting-line-position-in-edit-box-uicontrol/
        while isempty(jinfo) % Fix for R2018b - wtf...
            pause(0.01)
            jinfo = findjobj(handles.info); 
        end
        component = jinfo.getComponent(0).getComponent(0);
        
        oippriv.handles = handles;
        oippriv.handles.jinfo = component;
        oippriv.text = '';
        oippriv.caption = '';
        oippriv.progress = 0.0;
        oippriv.progressmax = 1.0;

        oippriv.progressbar = rectangle('Parent', handles.progress, 'Position', [0 0 0.0000000001 1], 'Edgecolor', 'none', 'Facecolor', 'b');
    end
end

function varargout = infopanelgui_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;
end
