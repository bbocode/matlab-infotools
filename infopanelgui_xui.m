function layout = infopanelgui_xui()
% Auto-generated using 'xui generate infopanelgui'
layout = ...
    xui.Window(...
        'Tag', 'infopanelgui', ...
        'Layout', 'Vertical', ...
        'Units', 'pixels', ...
        'SizeHint', [ 1024 600 ], ...
        'MinimumSize', [ 1024 600 ], ...
        'Title', 'Info Panel', ...
        'Children', {...
            xui.Widget(...
                'Tag', 'info', ...
                'Units', 'characters', ...
                'SizeHint', [ 146.286 29.7222 ], ...
                'Policy', '[Preferred Preferred]' ...
            ), ...
            xui.Widget(...
                'Tag', 'caption', ...
                'Units', 'characters', ...
                'SizeHint', [ 170.667 2.28571 ], ...
                'Policy', '[Preferred Fixed]' ...
            ), ...
            xui.Axes(...
                'Tag', 'progress', ...
                'Units', 'characters', ...
                'SizeHint', [ 170.667 1.07143 ], ...
                'Policy', '[Preferred Fixed]' ...
            ) ...
        } ...
    );
end
