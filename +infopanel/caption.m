function caption(varargin)
    global oippriv;
    infopanel.get();

    oippriv.caption = sprintf(varargin{:});
   
    infopanel.update();
end