function update()
    global oippriv;
    infopanel.get();
    
    % progress
    r = oippriv.progress/oippriv.progressmax;
    if r == 0,
        r = 1e-6;
    end
    set(oippriv.progressbar, 'Position', [0 0 r 1]);

    % caption
    set(oippriv.handles.caption, 'String', oippriv.caption);
    
    % info
    set(oippriv.handles.info, 'String', oippriv.text);
    
    drawnow;
    oippriv.handles.jinfo.setCaretPosition(oippriv.handles.jinfo.getDocument.getLength());
end
