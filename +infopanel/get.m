function h = get()
   persistent iphandle;
   
   if isempty(iphandle)
       iphandle = infopanelgui();
   end
   
   h = iphandle;
end