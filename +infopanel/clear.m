function clear()
    global oippriv;
    infopanel.get();

    oippriv.text = '';
    oippriv.caption = '';
    oippriv.progress = 0.0;
    oippriv.progressmax = 1.0;
   
    infopanel.update();
end