function progress(val, valmax)
    global oippriv;
    infopanel.get();

    oippriv.progress = val;
    oippriv.progressmax = valmax;
   
    infopanel.update();
end