function printf(varargin)
    global oippriv;
    infopanel.get();
    line = sprintf(varargin{:});
    oippriv.text = [oippriv.text line];
    infopanel.update();
end
